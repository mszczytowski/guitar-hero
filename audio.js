$(function() {

  var TRESHOLD = 70;

var webaudio_tooling_obj = function () {
  var stats = require('statsjs');

  function log10(x) {
      return Math.log(x)/Math.LN10;
  }

  function median(values) {
    var nums = stats(values);
    //document.getElementById("volume-text-q3").innerText = nums.q3();
    return nums.q3();
  }

  function median2(values) {
    values.sort( function(a,b) {return a - b;} );

    var half = Math.floor(values.length/2);

    if(values.length % 2)
        return values[half];
    else
        return (values[half-1] + values[half]) / 2.0;
  }

  var maxVolume = 0;
  var averages = [];
  var mean = 0;
  var averagesGraph = [];

  var array_float_frequency, array_freq_domain;

  var audioContext = new AudioContext();

  console.log("audio is starting up ...");

  var BUFF_SIZE_RENDERER = 16384;

  var audioInput = null,
  microphone_stream = null,
  gain_node = null,
  script_processor_node = null,
  script_processor_analysis_node = null,
  analyser_node = null;


  if (!navigator.getUserMedia)
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia || navigator.msGetUserMedia;

  if (navigator.getUserMedia){
      navigator.getUserMedia({audio:true},
          function(stream) {
              start_microphone(stream);
          },
          function(e) {
              alert('Error capturing audio.');
          });
  } else {
    alert('getUserMedia not supported in this browser.');
  }

  function process_microphone_buffer(event) {
      var i, N, inp, microphone_output_buffer;
      microphone_output_buffer = event.inputBuffer.getChannelData(0); // just mono - 1 channel for now
  }

  function show_some_data(given_typed_array, num_row_to_display, label) {

      var size_buffer = given_typed_array.length;
      var index = 0;

      console.log("__________ " + label);

      if (label === "time") {

          for (; index < num_row_to_display && index < size_buffer; index += 1) {

              var curr_value_time = (given_typed_array[index] / 128) - 1.0;

              console.log(curr_value_time);
          }

      } else if (label === "frequency") {

          for (; index < num_row_to_display && index < size_buffer; index += 1) {

              console.log(given_typed_array[index]);
          }

      } else {

          throw new Error("ERROR - must pass time or frequency");
      }
  }


  function start_microphone(stream){
      gain_node = audioContext.createGain();
      gain_node.connect( audioContext.destination );

      microphone_stream = audioContext.createMediaStreamSource(stream);
      microphone_stream.connect(gain_node);

      script_processor_node = audioContext.createScriptProcessor(BUFF_SIZE_RENDERER, 1, 1);
      script_processor_node.onaudioprocess = process_microphone_buffer;

      microphone_stream.connect(script_processor_node);
      // --- enable volume control for output speakers

      /*document.getElementById('volume').addEventListener('change', function() {

          var curr_volume = this.value;
          gain_node.gain.value = curr_volume;

          console.log("curr_volume ", curr_volume);
      });*/

      // --- setup FFT

      script_processor_analysis_node = audioContext.createScriptProcessor(2048, 1, 1);
      script_processor_analysis_node.connect(gain_node);

      analyser_node = audioContext.createAnalyser();
      analyser_node.smoothingTimeConstant = 0;
      analyser_node.fftSize = 2048;

      microphone_stream.connect(analyser_node);

      analyser_node.connect(script_processor_analysis_node);

      var buffer_length = analyser_node.frequencyBinCount;

      array_freq_domain = new Uint8Array(buffer_length);
      var array_time_domain = new Uint8Array(buffer_length);
      array_float_frequency = new Float32Array(buffer_length);

      script_processor_analysis_node.onaudioprocess = function() {

          // get the average for the first channel
          analyser_node.getByteFrequencyData(array_freq_domain);
          analyser_node.getFloatFrequencyData(array_float_frequency);
          analyser_node.getByteTimeDomainData(array_time_domain);

          //decibel_level = 10 * log10( gain.value );
          //show_some_data(array_freq_domain, 5, "frequency");
          //console.log(decibel_level);
      };
      draw();
  }


  var WIDTH = 300;
  var HEIGHT = 300;

  function draw() {
      var dataArray = array_float_frequency;
      var bufferLength = dataArray.length;
      drawVisual = requestAnimationFrame(draw);


      var sliceWidth = WIDTH * 1.0 / bufferLength;
      var x = 0;

      for(var i = 0; i < bufferLength; i++) {
        //console.log(dataArray[i]);
        var v = dataArray[i] / 128.0;
        var y = Math.abs(v * HEIGHT/2 + HEIGHT/2);

        x += sliceWidth;
      }

      //

      var stats = require('statsjs');

      // var values = 0;
      // var length = array_freq_domain.length;
      // for (var i = 0; i < length; i++) {
      //     values += array_freq_domain[i];
      // }

      // var average = values / length;
      var average = stats(array_freq_domain).q3();
      if(maxVolume < average) {
        maxVolume = average;
      }

      averages.push(average);
      averagesGraph.push(average);

      if(averagesGraph.length > 50) {
        averagesGraph.shift();
      }
    };

    setInterval(function(){
      mean = median(averages.slice(-20));
      /*var event = new CustomEvent('volume', {
        value: mean/70
      });
      this.dispatchEvent(event)*/
      $( "#wrapper1" ).trigger( "volume", [mean/TRESHOLD]);
    }, 50);

  }();
});
