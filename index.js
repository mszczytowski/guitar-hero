var five = require("johnny-five");
var server = require('http').createServer();
var io = require('socket.io')(server);
var keypress = require('keypress');

keypress(process.stdin);

io.on('connection', function(socket) {
    socket.on('shock', function() {
        console.log('shock!!!!');
    });
    socket.on('disconnect', function() {
        console.log('disconnect');
    });

    process.stdin.on('keypress', function(ch, key) {
        if (key && key.name === 'space') {
              console.log(key.name);
              socket.emit('sound1', { level: 1.0 });
              setTimeout(function () {
                  socket.emit('sound1', { level: 0.0 });
              }, 200);
        }
        if (key && key.name === 'z') {
              console.log(key.name);
              socket.emit('sound2', { level: 1.0 });
              setTimeout(function () {
                  socket.emit('sound2', { level: 0.0 });
              }, 200);
        }
        if (key && key.ctrl && key.name == 'c') {
            console.log('sdtin freed!');
            process.stdin.setRawMode(false);
        }
    });

    process.stdin.setRawMode(true);
    process.stdin.resume();
});

server.listen(3000);
