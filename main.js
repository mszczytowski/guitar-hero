$(function() {
  //var jebut = require('./jebut.js');

  var player = function (playerNumber, host) {
    var socket = io(host);

    var width = 400;
    var height = 600;
    var barWidth = 100;
    var barHeight = 500;
    var speed = 2;
    var maxItems = 3;
    var sound = false;
    var offset = 0;
    var points = 0;
    var fails = 0;
    var hitLine;
    var barMarker;
    var running = false;
    var interval;
    var current = -1;

    d3.select("#wrapper" + playerNumber).attr("style", "perspective: " + width + "px; width: " + width + "px; height: " + height + "px");
    d3.select("#board" + playerNumber).attr("style", "transform: rotateX(60deg)");

    var line = d3.line().x(function(d){return d[0];}).y(function(d){return d[1];});
    var board = d3.select("#board" + playerNumber).append("svg:svg").attr("width", width).attr("height", height);
    var boardGroup = board.append("svg:g").attr('perspective', '145 10 -230').attr('transform', 'translate(10 40) rotateY(-20 0 50)');
    var bar = d3.select("#bar").append("svg:svg").attr("width", barWidth).attr("height", barHeight);

    var data = [
      0,
      1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1,
      1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0,
      1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0,
      1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1,
      1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1,
      1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1,
      1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1
    ].map(function(x) {
      return [x === 1, 0];
    });

    var maxPoints = data.filter(function(x) {
      return x[0];
    }).length;

    console.log('maxPoints: ' + maxPoints);

    prepare();

    $( "#wrapper1" ).on('volume', function (event, value) {
      if(value >= 1) {
        if(!sound) {
          markSound();
        }
        sound = true;
      } else {
        sound = false;
      }
      setHitLineColor(0);
      setBarMarker(value);
    });

    /*socket.on('volume', function (data) {
      if(data.value >= 1) {
        if(!sound) {
          markSound();
        }
        sound = true;
      } else {
        sound = false;
      }
      setHitLineColor(0);
      setBarMarker(data.value);
    });*/

    $('body').keypress(function(event) {
      if(event.which === 32) {
          if(!running) {
            drawStrong('Do your best!');
            running = true;
            offset = 0;
            points = 0;
            fails = 0;
            speed = 2;
            interval = setInterval(repaint, 10);
          }
      }
    });

    function repaint() {
      boardGroup.selectAll(".marker").remove();

      offset += speed;

      var lastHeight = 0;

      for(var i = 0; i < data.length; i++) {
        if(data[i][0]) {
          var markerHeight = (height * 0.05) - i * height/maxItems + offset;

          lastHeight = markerHeight;

          boardGroup.append("svg:path")
              .attr("d", line([[20, markerHeight], [width - 20, markerHeight]]))
              .attr("stroke", "#7381AC")
              .attr("stroke-width", 40)
              .attr("fill", "none")
              .attr("class", "marker");

         
          if(markerHeight > 0.8 * height && markerHeight < height) {
            current = i;
          }

          if(markerHeight >= height && current === i) {
            current = -1;
          }

          if(markerHeight >= height && data[i][1] === 0) {
            setHitLineColor(-1);
            //addFails(1);
            data[i][1] = -1;
          }

        }
      }

      if(lastHeight > height*1.1) {
        drawStrong(points > (maxPoints * 0.5) ? 'Great job' : 'That was really bad');
        clearInterval(interval);
        running = false;
        interval = null;
      }
    }

    function prepare() {
      // left line
      boardGroup.append("svg:path")
          .attr("d", line([[0, 0], [0, height]]))
          .attr("stroke", "white")
          .attr("stroke-width", 3)
          .attr("fill", "none");

      // right line
      boardGroup.append("svg:path")
          .attr("d", line([[width, 0], [width, height]]))
          .attr("stroke", "white")
          .attr("stroke-width", 3)
          .attr("fill", "none");

      // lines
      for(var i = 0; i < 20; i++) {
        var lineHeight = parseInt(height - (i * height/17), 10);
        if(i == 19) { lineHeight = 2; }

        boardGroup.append("svg:path")
            .attr("d", line([[0, lineHeight], [width, lineHeight]]))
            .attr("stroke", "gray")
            .attr("stroke-width", 1)
            .attr("fill", "none");
      }

      // hit line
      hitLine = boardGroup.append("svg:path")
          .attr("d", line([[0, 0.94*height], [width, 0.94*height]]))
          .attr("stroke", "white")
          .attr("stroke-width", 10)
          .attr("fill", "none");

      bar.append("svg:path")
              .attr("d", line([[0, 0], [0, barHeight]]))
              .attr("stroke", "rgba(0, 255, 0, 0.15)")
              .attr("stroke-width", 3)
              .attr("fill", "none");

      bar.append("svg:path")
              .attr("d", line([[barWidth, 0], [barWidth, barHeight]]))
              .attr("stroke", "rgba(0, 255, 0, 0.15)")
              .attr("stroke-width", 3)
              .attr("fill", "none");

      bar.append("svg:path")
              .attr("d", line([[0, 0], [barWidth, 0]]))
              .attr("stroke", "rgba(0, 255, 0, 0.15)")
              .attr("stroke-width", 3)
              .attr("fill", "none");

      bar.append("svg:path")
              .attr("d", line([[0, barHeight], [barWidth, barHeight]]))
              .attr("stroke", "rgba(0, 255, 0, 0.15)")
              .attr("stroke-width", 3)
              .attr("fill", "none");


      bar.append("svg:path")
              .attr("d", line([[0, barHeight/2], [barWidth, barHeight/2]]))
              .attr("stroke", "white")
              .attr("stroke-width", 3)
              .attr("fill", "none");

      barMarker = bar.append("svg:path")
          .attr("d", line([[0.01*barWidth, 0.5*barHeight], [0.99*barWidth, 0.5*barHeight]]))
          .attr("stroke", "green")
          .attr("stroke-width", 20)
          .attr("fill", "none")
          .attr("class", "bar-marker");
    }

    function drawStrong(text) {
      d3.select("#comment" + playerNumber).text(text);
    }

    function addPoints(p) {
      if(!running) {
        return;
      }
      points += p;
      if(points % 5 === 0) {
        drawStrong('Do it faster!');
        speed += 0.2;
      } else {
        drawStrong('I like it!');
      }
      d3.select(".points" + playerNumber).text(points);
    }

    function setHitLineColor(state) {
      if(state === -1) {
        hitLine.attr('stroke', '#AA3C39');
        setTimeout(setHitLineColor, 150);
      } else if(state === 1) {
        hitLine.attr('stroke', '#2A7E43');
        setTimeout(setHitLineColor, 150);
      } else if (sound) {
        hitLine.attr('stroke', '#AA9939');
      } else {
        hitLine.attr('stroke', 'white');
      }
    }

    function setBarMarker(value) {
        var height = value/2;
        height = (height > 1)? 1 : height;
        bar.selectAll(".bar-marker").remove();
        bar.append("rect")
            .attr("x", 0)
            .attr("y", (1-height)*barHeight)
            .attr("width", barWidth)
            .attr("height", height*barHeight)
            .attr("fill", d3.interpolateWarm(height))
            .attr("class", "bar-marker");
    }

    function markSound() {
      console.log('current ' + current);
      if(current > -1) {
        if(data[current][1] === 0) {
          data[current][1] = 1;
          fails = 0;
          setHitLineColor(1);
          addPoints(1);
        }
      } else {
        addFails(1);
      }
    }

    function addFails(f) {
      if(!running) {
        return;
      }
      fails += f;
      if((fails+1) % 10) {
        drawStrong('My ears are bleeding!');
        socket.emit('jebut');
        // throw new Error("JEBUTŁO!");
        //jebut();
      } else if(fails > 2) {
        drawStrong('What are you doing?!');
      } else {
        drawStrong('Noooooo...');
      }
    }
  };

  player(1, 'http://127.0.0.1:9999');
  // player(2, 'http://10.70.23.14:9999');
});
