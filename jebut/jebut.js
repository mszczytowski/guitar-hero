var five = require("johnny-five");
var board = new five.Board();

var mTimeout =  false;

var jebut  = function() {
  board.digitalWrite(7, 5);
  mTimeout = setTimeout(function(){board.digitalWrite(7, 0)},1000);
}

board.on("ready", function() {
 var piezo = new five.Piezo(7);
 board.digitalWrite(7, 0);
 var _self = this;
 setTimeout(function() {console.log("Hit") ; jebut(_self)}, 1000);
});


var app = require('http').createServer(handler);
var fs = require('fs');

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

var io = require('socket.io')(app);
var socket = null;

app.listen(9999);
io.on('connection', function (s) {
  s.on('jebut', function (data) {
    jebut();
  });
});
